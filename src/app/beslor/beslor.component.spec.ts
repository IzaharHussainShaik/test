import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeslorComponent } from './beslor.component';

describe('BeslorComponent', () => {
  let component: BeslorComponent;
  let fixture: ComponentFixture<BeslorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeslorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeslorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
